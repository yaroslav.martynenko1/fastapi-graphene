FROM python:3.8

WORKDIR /code

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /usr/src

COPY Pipfile Pipfile.lock ./

RUN pip install --upgrade pip
RUN pip install pipenv
# install dependencies
RUN pipenv install --system --ignore-pipfile

# copy project
COPY ./app ./app

ENTRYPOINT [ "uvicorn", "app.main:app", "--host", "0.0.0.0"]