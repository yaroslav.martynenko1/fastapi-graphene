from odmantic.bson import ObjectId
import graphene
from graphene_file_upload.scalars import Upload

from app.types import PageType
from app.settings import engine, file_storage, DOMAIN
from app.models import Page
from app.services import ImageSaver


async def get_page_by_id(id: str):
    page = await engine.find_one(Page, Page.id == ObjectId(id))
    if page is None:
        raise ValueError(f'No page with id:{id}')
    return page


class CreatePage(graphene.Mutation):
    class Arguments:
        title = graphene.String(required=True)
        meta = graphene.String(default_value='')
        text = graphene.String(default_value='')
        images = Upload()

    page = graphene.Field(PageType)

    @staticmethod
    async def mutate(root, info, **input):
        image_saver = ImageSaver(file_storage)
        image_files = input.pop('images', [])
        page = Page(**input)
        await engine.save(page)
        images = [image_saver.save_image(image, page.id) for image in image_files]
        page.images.extend(images)
        await engine.save(page)
        return CreatePage(page=page)


class UpdatePage(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)
        title = graphene.String()
        meta = graphene.String()
        text = graphene.String()
        images = Upload()

    page = graphene.Field(PageType)

    @staticmethod
    async def mutate(root, info, id: str, **input):
        page = await get_page_by_id(id)
        image_saver = ImageSaver(file_storage)
        image_files = input.pop('images', [])
        images = [image_saver.save_image(image, page.id) for image in image_files]
        page.images.append(images)
        await engine.save(page)
        return UpdatePage(page=page)


class DeletePage(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)

    ok = graphene.Boolean()

    @staticmethod
    async def mutate(root, info, id: str):
        page = await get_page_by_id(id)
        image_saver = ImageSaver(file_storage)
        for image in page.images:
            image_saver.delete_image(image.name)
        await engine.delete(page)
        return DeletePage(ok=True)


class DeletePageImages(graphene.Mutation):
    class Arguments:
        id = graphene.ID(required=True)
        images = graphene.List(graphene.String, required=True)

    ok = graphene.Boolean()

    @staticmethod
    async def mutate(root, info, id: str, images: str):
        page = await get_page_by_id(id)
        image_saver = ImageSaver(file_storage)
        new_images = []
        for image in page.images:
            if image.name not in images: 
                new_images.append(image)
            else:
                image_saver.delete_image(image.name)
        page.images = new_images
        await engine.save(page)
        return DeletePageImages(ok=True)


class Mutation(graphene.ObjectType):
    create_page = CreatePage.Field()
    update_page = UpdatePage.Field()
    delete_page = DeletePage.Field()
    delete_page_images = DeletePageImages.Field()


class Query(graphene.ObjectType):
    page = graphene.Field(PageType, id=graphene.ID(required=True))
    pages = graphene.List(PageType)

    async def resolve_page(self, info, id: str):
        page = await engine.find_one(Page, Page.id == ObjectId(id))
        return page

    async def resolve_pages(self, info):
        pages = await engine.find(Page)
        return pages


schema=graphene.Schema(
    query=Query,
    mutation=Mutation,
)