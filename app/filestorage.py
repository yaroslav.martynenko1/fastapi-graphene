from io import FileIO
from os import path, mkdir, remove
from shutil import copyfileobj

from app.services import FileStorageInterface
from app import settings


class LocalStorage(FileStorageInterface):

    def __init__(self, storage_path) -> None:
        self._storage_path = storage_path
        if not path.isdir(self._storage_path):
            mkdir(self._storage_path)

    def save(self, filename: str, file: FileIO) -> str:
        filepath = self.get_path(filename) 
        with open(filepath,'wb') as to_file:
            copyfileobj(file, to_file)
        return self.get_url(filename)

    def delete(self, filename: str):
        remove(self.get_path(filename))

    def get_path(self, filename: str) -> str:
        return path.join(self._storage_path,filename) 
        
    def get_url(self, filename: str) -> str:
        return f'{settings.DOMAIN}{settings.MEDIA_URL}{filename}'