import os
from odmantic import AIOEngine
from motor.motor_asyncio import AsyncIOMotorClient

from app.filestorage import LocalStorage


BASE_DIR = os.path.dirname(os.path.abspath(__file__))

DOMAIN = os.environ.get('DOMAIN', 'http://localhost:8000')

GRAPHIQL = bool(os.environ.get('GRAPHIQL', False))

#media
MEDIA_URL = '/media/'
MEDIA_ROOT = 'media'

# DB settings
DATABASE = os.environ.get('DATABASE')
DB_CLIENT = os.environ.get('DB_CLIENT')

client = AsyncIOMotorClient(DB_CLIENT)
engine = AIOEngine(client, database=DATABASE)

# FILE STORAGE
file_storage = LocalStorage(os.path.join(BASE_DIR, MEDIA_ROOT))