from os import name
import graphene


class ImageType(graphene.ObjectType):
    name = graphene.String()
    url = graphene.String()


class PageType(graphene.ObjectType):
    id = graphene.ID()
    title = graphene.String()
    meta = graphene.String()
    images = graphene.List(ImageType)
    text = graphene.String()
