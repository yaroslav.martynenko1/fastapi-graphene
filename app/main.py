from graphene.types import schema
from graphql.execution.executors.asyncio import AsyncioExecutor
from fastapi import FastAPI
from fastapi.responses import FileResponse

from app.customapp import CustomGraphqlApp
from app.schema import schema
from app.settings import MEDIA_ROOT, file_storage, MEDIA_URL, GRAPHIQL


app = FastAPI()
app.add_route("/graphql/", CustomGraphqlApp(
    schema = schema,
    executor_class=AsyncioExecutor,
    graphiql=GRAPHIQL
))

@app.get(f"{MEDIA_URL}"+"{filename}")
async def get_media(filename: str):
    return FileResponse(file_storage.get_path(filename))