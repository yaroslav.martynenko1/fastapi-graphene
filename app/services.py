import os
from abc import ABC, abstractmethod
from io import FileIO
from typing import List

from starlette.datastructures import UploadFile

from app.models import Image

class FileStorageInterface(ABC):
    
    @abstractmethod
    def save(self, filename: str, file: FileIO) -> str:
        pass

    @abstractmethod
    def get_url(self, filename: str) -> str:
        pass

    @abstractmethod
    def delete(self, filename: str):
        pass


class ImageSaver:
    
    def __init__(self, storage: FileStorageInterface) -> None:
        self._storage = storage

    def _create_filename(self, image_name: str, page_id: str):
        return f'{str(page_id)}_{image_name}'

    def save_image(self, image: UploadFile, page_id: str) -> Image:
        #TODO add validation that file is image and filename validation
        filename = self._create_filename(image.filename, page_id)
        url = self._storage.save(filename, image.file)
        return Image(name=filename, url=url)

    def delete_image(self, image_name: str):
        self._storage.delete(image_name)