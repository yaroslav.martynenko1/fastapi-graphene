
from typing import List
from odmantic import Model
from pydantic import BaseModel

class Image(BaseModel):
    name: str
    url: str


class Page(Model):
    title: str
    meta: str = ""
    text: str = ""
    images: List[Image] = []
